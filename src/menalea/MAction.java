package menalea;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public abstract class MAction extends AbstractAction {

	public MAction(String name) {
		super(name);
	}

	public MAction(String name, KeyStroke keyStroke) {
		super(name);
		putValue(ACCELERATOR_KEY, keyStroke);
		putValue(SHORT_DESCRIPTION, name + " (" +
				keyStrokeToString(keyStroke) + ")");
	}

	public MAction(String name, Icon icon) {
		super(name, icon);
		putValue(SHORT_DESCRIPTION, name);
	}

	public MAction(String name, KeyStroke keyStroke, Icon icon) {
		super(name, icon);
		putValue(ACCELERATOR_KEY, keyStroke);
		putValue(SHORT_DESCRIPTION, name + " (" +
				keyStrokeToString(keyStroke) + ")");
	}

	public MAction(String name, Icon icon, String descr) {
		super(name, icon);
		putValue(SHORT_DESCRIPTION, descr);
	}

	public MAction(String name, KeyStroke keyStroke, Icon icon, String descr) {
		super(name, icon);
		putValue(ACCELERATOR_KEY, keyStroke);
		putValue(SHORT_DESCRIPTION, descr + " (" +
				keyStrokeToString(keyStroke) + ")");
	}



	private static String keyStrokeToString(KeyStroke keyStroke) {
		// Yes, it's a bit hacky but it works.
		return keyStroke.toString()
				.replace("pressed", "")
				.trim()
				.replace("ctrl", "Ctrl")
				.replace("shift", "Maj")
				.replace("alt", "Alt")
				.replace("  ", "+")
				.replace(" ", "+");
	}
}
