package menalea;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;


public class Editor implements DocumentListener {

	/**
	 * Nullable
	 */
	private String filePath;

	public final JTextComponent textComponent;

	private boolean saved;

	private final List<Listener> listeners = new ArrayList<Listener>();



	public Editor(JTextComponent textComponent) {
		this.textComponent = textComponent;
		textComponent.getDocument().addDocumentListener(this);
		saved = true;
	}

	public Editor(JTextComponent textComponent, String filePath)
			throws IOException {
		this.textComponent = textComponent;
		open(filePath);
	}



	public void addListener(Listener listener) {
		listeners.add(listener);
	}

	/**
	 * 
	 * @return The file path or <code>null</code>.
	 */
	public String getFilePath(){
		return filePath;
	}

	public boolean isSaved() {
		return saved;
	}

	public void newDocument() {
		filePath = null;
		textComponent.setText("");
		saved = true;

		for (Listener listener : listeners) {
			listener.editorNewDocumentCreated(this);
		}
	}

	public void open(String filePath) throws IOException {
		textComponent.setText(Tools.readUTF8File(filePath));
		this.filePath = filePath;
		saved = true;

		for (Listener listener : listeners) {
			listener.editorOpened(this);
		}
	}

	public void save() throws IOException {

		if (filePath == null) {
			throw new RuntimeException("The document has no path");
		}

		Tools.writeUTF8File(new File(filePath), textComponent.getText());

		saved = true;

		for (Listener listener : listeners) {
			listener.editorSaved(this);
		}
	}

	public void saveAs(String filePath) throws IOException {
		Tools.writeUTF8File(new File(filePath), textComponent.getText());
		saved = true;
		this.filePath = filePath;

		for (Listener listener : listeners) {
			listener.editorSaved(this);
		}
	}



	@Override
	public void changedUpdate(DocumentEvent e) {
		saved = false;
		for (Listener listener : listeners) {
			listener.editorTextChanged(this);
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		changedUpdate(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		changedUpdate(e);
	}



	public static interface Listener {
		public void editorSaved(Editor editor);
		public void editorTextChanged(Editor editor);
		public void editorOpened(Editor editor);
		public void editorNewDocumentCreated(Editor editor);
	}

}
