package menalea;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * A dialog to search and replace text.
 * @author antoine1023
 *
 */
@SuppressWarnings("serial")
public class SearchDialog extends JDialog {

	private JTextField searchTextField;
	private JTextField replaceTextField;
	private JCheckBox matchCaseChckbx;

	private final MTextArea textPane;

	public SearchDialog(final JFrame parent, MTextArea textPane) {

		super(parent);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setResizable(false);
		setTitle("Rechercher et remplacer");
		setBounds(100, 100, 600, 250);

		this.textPane = textPane;

		getContentPane().setLayout(new BorderLayout());

		{
			Box verticalBox = Box.createVerticalBox();
			verticalBox.setBorder(BorderFactory.createEmptyBorder(
					10, 10, 10, 10));
			getContentPane().add(verticalBox, BorderLayout.NORTH);
			{
				Box searchBox = Box.createHorizontalBox();
				verticalBox.add(searchBox);

				searchBox.add(new JLabel("Rechercher : "));

				Component horizontalStrut = Box.createHorizontalStrut(20);
				searchBox.add(horizontalStrut);

				searchTextField = new JTextField();
				searchBox.add(searchTextField);

			}

			{
				Component verticalStrut = Box.createVerticalStrut(10);
				verticalBox.add(verticalStrut);
			}

			{
				Box replaceBox = Box.createHorizontalBox();
				verticalBox.add(replaceBox);

				replaceBox.add(new JLabel("Remplacer par : "));

				Component horizontalStrut = Box.createHorizontalStrut(10);
				replaceBox.add(horizontalStrut);

				replaceTextField = new JTextField();
				replaceBox.add(replaceTextField);
			}

			verticalBox.add(Box.createVerticalStrut(20));

			{
				Box horizontalBox = Box.createHorizontalBox();

				matchCaseChckbx = new JCheckBox("Respecter la casse");
				matchCaseChckbx.setVerticalAlignment(SwingConstants.TOP);
				matchCaseChckbx.setHorizontalAlignment(SwingConstants.LEFT);
				matchCaseChckbx.setToolTipText("Respecter la casse " +
						"(le fait que les caractères soient en " +
						"majuscules ou en minuscules)");
				horizontalBox.add(matchCaseChckbx);

				horizontalBox.add(Box.createHorizontalGlue());
				verticalBox.add(horizontalBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);

			JButton closeButton = new JButton("Fermer");
			buttonPane.add(closeButton);
			closeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}
			});
			getRootPane().setDefaultButton(closeButton);

			JButton replaceAllButton = new JButton("Tout remplacer");
			replaceAllButton.setToolTipText(
					"Remplace toutes les occurences");
			replaceAllButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					replaceAll();
				}
			});
			buttonPane.add(replaceAllButton);

			JButton replaceButton = new JButton("Remplacer");
			replaceAllButton.setToolTipText(
					"Remplace l’occurences suivante");
			buttonPane.add(replaceButton);

			JButton searchButton = new JButton("Rechercher");
			searchButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					search();
				}
			});
			buttonPane.add(searchButton);

		}

	}

	//      private int getSelectionLength() {
	//              return Math.abs(textPane.getSelectionEnd()
	//                              - textPane.getSelectionStart());
	//      }

	private void replaceAll() {
		// TODO Works bad with the undo/redo

		String text = textPane.getText(),
				texteMin = text,
				target = searchTextField.getText(),
				remplacant = replaceTextField.getText();

		if (! matchCaseChckbx.isSelected()) {
			texteMin = text.toLowerCase();
			target = target.toLowerCase();
		}

		int pos = 0;
		for (;;) {

			pos = texteMin.indexOf(target, pos);
			if (pos == -1) break;

			String avant = text.substring(0, pos),
					apres = text.substring(pos + target.length());
			text = avant + remplacant + apres;

			if (! matchCaseChckbx.isSelected())
				texteMin = text.toLowerCase();
			else texteMin = text;
		}

		textPane.setText(text);
	}

	private void search() {

		String texte = textPane.getText(),
				cible = searchTextField.getText();

		if (! matchCaseChckbx.isSelected()) {
			texte = texte.toLowerCase();
			cible = cible.toLowerCase();
		}

		if (! texte.contains(cible)) {
			return;
		}

		int debutOccurence = searchNext(texte, cible,
				textPane.getSelectionStart());

		textPane.select(debutOccurence, debutOccurence + cible.length());
	}

	private static int searchNext(String text, String target, int offset) {

		int occurrenceStart = text.indexOf(target, offset + 1);

		if (occurrenceStart == -1)
			return searchNext(text, target, -1);

		return occurrenceStart;
	}

}