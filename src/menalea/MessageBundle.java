package menalea;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class MessageBundle extends HashMap<Message, String> {

	private static final long serialVersionUID = 3861521352402781177L;



	public MessageBundle(Map<? extends Message, ? extends String> map) {
		super(map);
	}



	public String get(Message message) {
		return super.get(message);
	}

	public String get(Message message, Object... arguments) {
		return MessageFormat.format(get(message), arguments);
	}

	@Override
	public String put(Message message, String string) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends Message,? extends String> m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String remove(Object key) {
		throw new UnsupportedOperationException();
	}

}