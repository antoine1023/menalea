package menalea;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;


/**
 * An enum of all the translated messages.
 *
 */
public enum Message {

	/* Command Line Interface */
	cliHelp,
	cliLang,
	cliRemovePreferences,



	addButton,
	cancelButton,
	editButton,
	removeButton,
	okButton,
	yesButton,
	noButton,
	closeButton,

	untitledFile,
	modifiedFile,

	saveConfirmationDialogText,



	menuFile,
	menuFileNew,
	menuFileOpen,
	menuFileSave,
	menuFileSaveAs,
	menuFileExport,
	menuFileHtmlPreview,
	menuFileExit,



	menuEdit,
	menuEditUndo,
	menuEditRedo,
	menuEditCopy,
	menuEditCut,
	menuEditPaste,
	menuEditFindReplace,
	menuEditSelectAll,



	menuFormat,
	menuFormatBold,
	menuFormatItalic,
	menuFormatStrike,
	menuFormatHeading,
	menuFormatHeadingDialogText,
	menuFormatProfileSwitcher,
	menuFormatShowProfileDialog,

	/** The name of the default read-only profile */
	profileReadOnlyDefaultName,
	/** The name of the plain read-only profile */
	profileReadOnlyNoStyleSheetName,

	profileListDialogTitle,
	profileListDialogNewProfileName,

	profileTypeChooserTitle,
	profileTypeChooserPleaseSelectAType,

	profileTypeSimple,
	profileTypeCustomStyleSheet,

	profileDialogTitle,
	profileDialogName,
	profileDialogCssRules,
	profileDialogFontSize,
	profileDialogFontFamily,
	profileDialogHeadingFontFamily,
	profileDialogCannotRemoveCurrent,



	menuView,
	menuViewZoomIn,
	menuViewZoomOut,



	menuHelp,
	menuHelpMarkdown,
	menuHelpMarkdownURL,
	menuHelpAbout,
	menuHelpAboutDialogText,
	menuHelpAboutDialogCopying,
	;



	private Message() {
	}



	public static MessageBundle getDefaults() {
		return get(Locale.getDefault());
	}


	private static MessageBundle get(String fileName) throws IOException {
		InputStream stream = Message.class.getResourceAsStream(fileName);
		if (stream == null) {
			throw new FileNotFoundException(fileName + " not found");
		}

		Properties properties = new Properties();

		properties.load(stream);
		return load(fileName, properties);
	}


	public static MessageBundle get(Locale locale) {

		String fileName = "messages";
		if (! locale.getLanguage().equals("en")) {
			fileName += "_" + locale.getLanguage();
		}
		fileName += ".properties";

		try {
			return get(fileName);
		} catch (IOException e) {
			if (locale.getLanguage().equals("en"))
				return null;
			return getEnglish();
		}
	}


	private static MessageBundle load(String fileName, Properties properties) {

		final Map<Message, String> msgToInt = new HashMap<Message, String>();
		final Map<String, Message> keyToMessage = toMap();

		for (Entry<Object, Object> entry : properties.entrySet()) {

			String key = (String) entry.getKey();
			String value = (String) entry.getValue();

			Message message = keyToMessage.remove(key);

			if (message == null) {
				throw new RuntimeException("Unknown key " + key);
			}

			msgToInt.put(message, value);
		}

		if (! keyToMessage.isEmpty()) {

			warn(fileName, "Keys " + keyToMessage.keySet() + " not found");

			for (Entry<String, Message> entry : keyToMessage.entrySet()) {
				String replacement = "KEY NOT FOUND (" + entry.getKey() + ")";
				msgToInt.put(entry.getValue(), replacement);
			}
		}

		return new MessageBundle(msgToInt);
	}


	public static MessageBundle getEnglish() {
		return get(Locale.ENGLISH);
	}


	public static Map<String, Message> toMap() {
		Map<String, Message> map = new HashMap<String, Message>();
		for (Message message : values()) {
			map.put(message.name(), message);
		}
		return map;
	}


	public static List<Message> toList() {
		return Arrays.asList(values());
	}


	public static String englishPropertiesToString() {
		String s = "";
		for (Message message : values()) {
			s += message.name() + "\n";
		}
		return s;
	}



	private static void warn(String fileName, String description) {
		System.err.println("Warning (in " + fileName + "): " + description);
	}
}
