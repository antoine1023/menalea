package menalea;

import java.awt.Component;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import menalea.profile.ProfileManager;

public class Tools {

	public static final FileNameExtensionFilter markdownFileFilter =
			new FileNameExtensionFilter("Document Markdown (*.md)", "md");

	public static final FileNameExtensionFilter htmlFileFilter =
			new FileNameExtensionFilter("Document HTML (*.html, *.htm)",
					"html", "htm");



	private Tools() {
	}



	private static JFileChooser getOpenFileChooser() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(markdownFileFilter);
		return chooser;
	}

	private static JFileChooser getSaveFileChooser() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(markdownFileFilter);
		return chooser;
	}

	private static JFileChooser getExportFileChooser() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(htmlFileFilter);
		return chooser;
	}


	private static File chooseFileSave(Component parent, JFileChooser chooser,
			String defaultExtension) {

		int returnValue = chooser.showSaveDialog(parent);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();

			String ext = getExtension(file.getName());
			if (ext != null && ext.equals(defaultExtension)) {
				return file;
			} else {
				return new File(file.getPath() + defaultExtension);
			}
		}
		return null;
	}

	private static File chooseFileOpen(Component parent, JFileChooser chooser) {

		int returnValue = chooser.showOpenDialog(parent);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile();
		}
		return null;
	}

	public static File chooseFileExport(Component parent) {
		return chooseFileSave(parent, getExportFileChooser(), ".html");
	}

	public static File chooseFileSave(Component parent) {
		return chooseFileSave(parent, getSaveFileChooser(), ".md");
	}

	public static File chooseFileOpen(Component parent) {
		return chooseFileOpen(parent, getOpenFileChooser());
	}



	public static String readUTF8(InputStream inputStream) throws IOException {

		InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8");
		BufferedReader br = new BufferedReader(isr);

		try {

			StringBuilder stringBuilder = new StringBuilder();
			String string;

			while ((string = br.readLine()) != null) {
				stringBuilder.append(string);
				stringBuilder.append('\n');
			}

			return stringBuilder.toString();

		} finally {
			br.close();
		}
	}


	public static String readUTF8File(String path) throws IOException {
		return readUTF8(new FileInputStream(path));
	}

	public static void writeUTF8(OutputStream os, String text)
			throws IOException {

		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		BufferedWriter bw = new BufferedWriter(osw);
		bw.write(text);
		bw.close();
	}

	public static void writeUTF8File(File file, String text)
			throws IOException {
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		writeUTF8(fileOutputStream, text);
	}

	/**
	 * 
	 * @param nom Le nom du fichier, extension ".png" ajoutée automatiquement
	 * @param descr Une courte description de l’image
	 * @return L’icone.
	 */
	public static ImageIcon getPNGIcon(String nom, String descr) {
		URL url = Tools.class.getResource("icons/" + nom + ".png");
		return new ImageIcon(url, descr);
	}

	public static ImageIcon getPNGIcon(String nom) {
		return getPNGIcon(nom, "");
	}


	public static void compileAndBrowseMarkdownPreview(String title,
			String markdown, ProfileManager profileManager)
					throws IOException {

		File temp = File.createTempFile("output", ".html");
		temp.deleteOnExit();

		String html = MarkdownConverter.convert(title, markdown,
				profileManager);

		Tools.writeUTF8File(temp, html);

		openWebpage(temp.toURI());
	}



	public static void openWebpage(URI uri) {
		Desktop desktop = null;
		if (Desktop.isDesktopSupported()) {
			desktop = Desktop.getDesktop();
		}

		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void openWebpage(URL url) {
		try {
			openWebpage(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}



	/**
	 * @return the extension of the file name or <code>null</code>.
	 */
	public static String getExtension(String fileName) {
		String ext = null;
		int i = fileName.lastIndexOf('.');

		if (i > 0 &&  i < fileName.length() - 1) {
			ext = fileName.substring(i + 1).toLowerCase();
		}
		return ext;
	}



	public static Properties getMenaleaProperties() throws IOException {
		String fileName = "menalea.properties";
		InputStream stream = Message.class.getResourceAsStream(fileName);
		if (stream == null) {
			throw new FileNotFoundException(fileName + " not found");
		}

		Properties properties = new Properties();
		properties.load(stream);
		return properties;
	}



	public static String getVersionString() {
		try {

			Properties properties = getMenaleaProperties();

			String major = properties.getProperty("versionMajor");
			String minor = properties.getProperty("versionMinor");
			String revision = properties.getProperty("versionRevision");

			return major + "." + minor + "." + revision;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
