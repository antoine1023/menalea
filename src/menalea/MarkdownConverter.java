package menalea;

import java.io.IOException;

import menalea.profile.Profile;
import menalea.profile.ProfileManager;

import com.github.rjeschke.txtmark.Processor;

public class MarkdownConverter {

	private static final String DOCTYPE = "<!DOCTYPE HTML PUBLIC " +
			"\"-//W3C//DTD HTML 4.01//EN\" " +
			"\"http://www.w3.org/TR/html4/strict.dtd\">";


	private MarkdownConverter() {
	}



	public static String convert(String title, String markdown,
			ProfileManager profileManager) throws IOException {
		return convert(title, markdown, profileManager.getCurrent());
	}

	public static String convert(String title, String markdown,
			Profile profile) throws IOException {
		return convert(title, markdown, profile.getCSS());
	}

	public static String convert(String title, String markdown, String cssRules)
			throws IOException {

		String body = "<body><div class=\"main-div\">" +
				Processor.process(markdown) + "</div></body>";

		String style = "<style type=\"text/css\">" + cssRules + "</style>";

		String head = "<head>" +
				style +
				"<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">" +
				"<title>" + title + "</title>" +
				"</head>";

		String html = DOCTYPE;
		html += "<html>\n" + head + "\n\n" + body + "\n</html>";

		return html;
	}
}
