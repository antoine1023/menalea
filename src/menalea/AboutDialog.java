package menalea;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class AboutDialog extends JDialog {

	// private final MessageBundle msgs;

	private final static String COPYRIGHT =
			"Copyright (C) 2013-2014 antoine1023";


	public AboutDialog(Window parent, MessageBundle msgs) {
		super(parent);

		// this.msgs = msgs;

		setTitle(msgs.get(Message.menuHelpAbout));
		setSize(600, 400);
		setLocationRelativeTo(null);

		{
			String text = "<center>";
			text += "<h1>Menalea " + Tools.getVersionString() + "</h1>";
			text += "</center><br>";

			text += msgs.get(Message.menuHelpAboutDialogText);
			text += "</center>";
			text += "<br><hr>";
			text += "<p>" + COPYRIGHT + "</p>";
			text += msgs.get(Message.menuHelpAboutDialogCopying);

			JTextPane textPane = new JTextPane();
			textPane.setContentType("text/html");
			textPane.setText(text);
			textPane.setEditable(false);

			final JScrollPane scrollPane = new JScrollPane(textPane);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					scrollPane.getVerticalScrollBar().setValue(0);
				}
			});

			add(scrollPane, BorderLayout.CENTER);
		}

		{
			JButton closeButton = new JButton(msgs.get(Message.closeButton));
			closeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}
			});
			add(closeButton, BorderLayout.SOUTH);
		}
	}


}
