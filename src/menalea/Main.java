package menalea;

import java.awt.Font;
import java.util.List;
import java.util.Locale;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;



public class Main {

	public final static String FONT_SIZE_KEY = "fontsize";
	public static final int DEFAULT_FONT_SIZE = 20;



	private static void setupUI() {
		// Empêche l’utilisation de DirectDraw sous Windows, permet de corriger
		// un problème de rafraichissement avec Aero.
		System.setProperty("sun.java2d.noddraw", Boolean.TRUE.toString());

		setLaF("Nimbus");

		Preferences preferences = Preferences.userNodeForPackage(Main.class);
		int size = (int) preferences.getFloat(FONT_SIZE_KEY, DEFAULT_FONT_SIZE);
		Font font = new Font(Font.SANS_SERIF, Font.PLAIN, size);

		UIDefaults props = UIManager.getLookAndFeelDefaults();
		props.put("defaultFont", font);
		props.put("Caret.width", 2);
	}



	private static void setLaF(String name) {
		try {

			LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();

			for (LookAndFeelInfo info : lafs) {
				if (name.equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}

		} catch (Exception e) {
			// Just ignore it, the default LaF will be used.
		}
	}



	private static Options createOptions(MessageBundle msgs) {
		Options options = new Options();

		options.addOption("h", "help", false, msgs.get(Message.cliHelp));
		options.addOption(null, "lang", true, msgs.get(Message.cliLang));
		options.addOption(null, "remove-preferences", false,
				msgs.get(Message.cliRemovePreferences));

		return options;
	}



	private static void open(List<String> files, MessageBundle msgs) {
		String filePath = null;
		if (files.size() >= 1) {
			filePath = files.get(0);
		}
		open(filePath, msgs);
	}


	/**
	 * 
	 * @param filePath The file path to open or <code>null</code>.
	 */
	private static void open(final String filePath, final MessageBundle msgs) {
		setupUI();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainWindow mainWindow = new MainWindow(msgs);
				mainWindow.setVisible(true);

				if (filePath != null) {
					mainWindow.open(filePath);
				}
			}
		});
	}



	/**
	 * @param args
	 */
	public static void main(final String[] args) {

		MessageBundle msgs = Message.getDefaults();

		Options options = createOptions(msgs);

		CommandLineParser parser = new GnuParser();
		try {
			CommandLine cmd = parser.parse(options, args, true);

			if (cmd.hasOption("lang")) {
				Locale locale = new Locale(cmd.getOptionValue("lang"));
				msgs = Message.get(locale);
				// rebuild options
				options = createOptions(msgs);
			}

			if (cmd.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("menalea", options);
				System.exit(0);
			}

			if (cmd.hasOption("remove-preferences")) {
				Preferences p = Preferences.userNodeForPackage(Main.class);
				try {
					p.removeNode();
				} catch (BackingStoreException e) {
					e.printStackTrace();
				}
				System.exit(0);
			}


			@SuppressWarnings("unchecked")
			List<String> files = cmd.getArgList();
			open(files, msgs);

		} catch (ParseException e) {
			System.out.println(e.getLocalizedMessage());
		}

	}

}
