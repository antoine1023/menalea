package menalea.profile;

import java.util.ArrayList;
import java.util.List;

import menalea.MessageBundle;


/**
 * An abstract style profile.
 *
 */
public abstract class EditableProfile extends Profile {

	private final List<Listener> listeners =
			new ArrayList<EditableProfile.Listener>();

	private String name;



	public EditableProfile() {
		this.name = "";
	}



	@Override
	public String toString() {
		return name;
	}


	/**
	 * @param msgs Can be <code>null</code>.
	 */
	@Override
	public final String getName(MessageBundle msgs) {
		return name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		notifyChange();
	}


	public abstract EditableProfileType getType();



	protected void notifyChange() {
		for (Listener l : listeners) {
			l.profileChanged(this);
		}
	}

	public void addListener(Listener listener) {
		listeners.add(listener);
	}
	public void removeListener(Listener listener) {
		listeners.remove(listener);
	}



	public static interface Listener {
		public void profileChanged(EditableProfile profile);
	}

}
