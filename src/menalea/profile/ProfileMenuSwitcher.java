package menalea.profile;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;

import menalea.Message;
import menalea.MessageBundle;

@SuppressWarnings("serial")
public class ProfileMenuSwitcher extends JMenu
implements ProfileManager.Listener {

	private final MessageBundle msgs;
	private final ProfileManager manager;



	public ProfileMenuSwitcher(
			ProfileManager profileManager,
			MessageBundle msgs) {

		manager = profileManager;
		this.msgs = msgs;

		setText(msgs.get(Message.menuFormatProfileSwitcher));
		manager.addListener(this);

		update();
	}

	public void dispose() {
		manager.removeListener(this);
	}



	private void update() {
		removeAll();

		ButtonGroup group = new ButtonGroup();

		for (final Profile profile : manager.getAllProfiles()) {
			JRadioButtonMenuItem radio = new JRadioButtonMenuItem();
			radio.setText(profile.getName(msgs));
			radio.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					manager.setCurrent(profile);
				}
			});
			group.add(radio);
			add(radio);

			if (manager.getCurrent() == profile) {
				radio.setSelected(true);
			}
		}
	}



	@Override
	public void profileAdded(ProfileManager manager) {
		update();
	}
	@Override
	public void profileRemoved(ProfileManager manager) {
		update();
	}
	@Override
	public void profileChanged(ProfileManager manager) {
		update();
	}

}
