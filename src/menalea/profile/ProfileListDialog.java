package menalea.profile;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import menalea.Message;
import menalea.MessageBundle;


/**
 * A dialog to edit editable profiles.
 *
 */
@SuppressWarnings("serial")
public class ProfileListDialog extends JDialog
implements ProfileManager.Listener {

	private final MessageBundle msgs;

	private final ProfileManager profileManager;

	private final JList profileList;
	private final DefaultListModel profileListModel;

	private final JButton editButton, addButton, removeButton, closeButton;



	public ProfileListDialog(JFrame parent,
			final ProfileManager profileManager, MessageBundle msgs) {

		super(parent, true);

		this.msgs = msgs;
		this.profileManager = profileManager;
		profileManager.addListener(this);


		setSize(400, 300);
		setMinimumSize(getSize());
		setTitle(msgs.get(Message.profileListDialogTitle));
		setLocationRelativeTo(null);


		setLayout(new BorderLayout());

		profileListModel = new DefaultListModel();
		profileList = new JList(profileListModel);
		profileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		addButton = new JButton(msgs.get(Message.addButton));
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addProfile();
			}
		});

		removeButton = new JButton(msgs.get(Message.removeButton));
		removeButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				removeSelectedProfile();
			}
		});

		editButton = new JButton(msgs.get(Message.editButton));
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editSelectedProfile();
			}
		});

		closeButton = new JButton(msgs.get(Message.closeButton));
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		getRootPane().setDefaultButton(closeButton);



		add(profileList, BorderLayout.CENTER);

		{
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
			buttonPanel.add(addButton);
			buttonPanel.add(removeButton);
			buttonPanel.add(editButton);
			add(buttonPanel, BorderLayout.EAST);
		}

		{
			JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonPanel.add(closeButton);
			add(buttonPanel, BorderLayout.SOUTH);
		}


		updateList();
	}



	@Override
	public void dispose() {
		profileManager.removeListener(this);
		super.dispose();
	}



	private void updateList() {
		profileListModel.removeAllElements();
		for (EditableProfile profile : profileManager.getEditableProfiles()) {
			profileListModel.addElement(profile);
		}
	}



	private EditableProfile getSelectedProfile() {
		return (EditableProfile) profileList.getSelectedValue();
	}


	private void addProfile() {
		EditableProfileType type = EditableProfileTypeChooser.showDialog(
				ProfileListDialog.this, msgs);

		if (type != null) {
			EditableProfile profile = profileManager.add(type,
					msgs.get(Message.profileListDialogNewProfileName));
			profileList.setSelectedValue(profile, true);
			editSelectedProfile();
		}
	}

	private void editSelectedProfile() {
		EditableProfile p = getSelectedProfile();
		if (p == null)
			return;

		ProfileDialog.editProfile(this, p, msgs);
	}

	private void removeSelectedProfile() {
		EditableProfile p = getSelectedProfile();
		if (p == null)
			return;
		if (profileManager.getCurrent() == p){
			String m = msgs.get(Message.profileDialogCannotRemoveCurrent);
			JOptionPane.showMessageDialog(this, m, "Menalea",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		profileManager.remove(p);
	}



	@Override
	public void profileAdded(ProfileManager manager) {
		updateList();
	}

	@Override
	public void profileRemoved(ProfileManager manager) {
		updateList();
	}

	@Override
	public void profileChanged(ProfileManager manager) {
		updateList();
	}

}
