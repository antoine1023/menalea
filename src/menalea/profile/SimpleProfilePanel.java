package menalea.profile;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import menalea.Message;
import menalea.MessageBundle;

@SuppressWarnings("serial")
public class SimpleProfilePanel extends ProfilePanel {

	private final SimpleProfile profile;

	private final JSpinner fontSizeSpinner;

	/**
	 * The text field of the text font.
	 */
	private final JTextField fontTextField;

	/**
	 * The text field of the heading font.
	 */
	private final JTextField headingFontTextField;



	public SimpleProfilePanel(SimpleProfile profile, MessageBundle msgs) {
		this.profile = profile;

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		fontSizeSpinner = new JSpinner();
		fontSizeSpinner.setPreferredSize(new Dimension(100, 40));

		fontTextField = new JTextField(10);
		headingFontTextField = new JTextField(10);


		{
			JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			p.add(new JLabel(msgs.get(Message.profileDialogFontSize)));
			p.add(fontSizeSpinner);
			add(p);
		}

		{
			JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			p.add(new JLabel(msgs.get(Message.profileDialogFontFamily)));
			p.add(fontTextField);
			add(p);
		}

		{
			JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			p.add(new JLabel(msgs.get(Message.profileDialogHeadingFontFamily)));
			p.add(headingFontTextField);
			add(p);
		}

		add(Box.createVerticalGlue());
	}



	@Override
	public EditableProfile getProfile() {
		return profile;
	}


	@Override
	public void apply() {
		profile.setFontSize((Integer) fontSizeSpinner.getValue());
		profile.setFontFamily(fontTextField.getText());
		profile.setHeadingFontFamily(headingFontTextField.getText());
	}


	@Override
	public void update() {
		fontSizeSpinner.setValue((int) profile.getFontSize());
		fontTextField.setText(profile.getFontFamily());
		headingFontTextField.setText(profile.getHeadingFontFamily());
	}
}
