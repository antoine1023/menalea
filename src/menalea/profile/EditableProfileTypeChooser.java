package menalea.profile;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import menalea.Message;
import menalea.MessageBundle;


@SuppressWarnings("serial")
public class EditableProfileTypeChooser extends JDialog {

	private final MessageBundle msgs;

	private final JComboBox comboBox;

	private final JButton okButton, cancelButton;

	private boolean canceled;



	private EditableProfileTypeChooser(Window parent, MessageBundle msgs) {
		super(parent, ModalityType.APPLICATION_MODAL);

		this.msgs = msgs;

		setLocationRelativeTo(null);
		setSize(500, 200);
		setMinimumSize(getSize());
		setTitle(msgs.get(Message.profileTypeChooserTitle));

		setLayout(new BorderLayout());

		comboBox = new JComboBox(EditableProfileType.values());
		comboBox.setRenderer(new ProfileTypeRenderer());


		okButton = new JButton(msgs.get(Message.okButton));
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		getRootPane().setDefaultButton(okButton);

		cancelButton = new JButton(msgs.get(Message.cancelButton));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				canceled = true;
			}
		});


		{
			Box box = Box.createVerticalBox();

			{
				JPanel p = new JPanel();
				p.setLayout(new BorderLayout());

				String text = msgs.get(
						Message.profileTypeChooserPleaseSelectAType);
				JLabel lbl = new JLabel(
						text, JLabel.LEFT);
				p.add(lbl);

				box.add(p);
			}

			box.add(Box.createVerticalGlue());

			box.add(comboBox);

			box.add(Box.createVerticalGlue());

			add(box, BorderLayout.CENTER);
		}

		{
			JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonPanel.add(cancelButton);
			buttonPanel.add(okButton);
			add(buttonPanel, BorderLayout.SOUTH);
		}
	}



	/**
	 * Shows a {@link JDialog} allowing the user to choose a
	 * {@link EditableProfileType}.
	 * 
	 * @return A {@link EditableProfileType} or <code>null</code>.
	 */
	public static final EditableProfileType showDialog(Window parent,
			MessageBundle msgs) {

		EditableProfileTypeChooser dialog = new EditableProfileTypeChooser(parent, msgs);
		dialog.setVisible(true);

		if (dialog.canceled)
			return null;
		else {
			Object[] selected = dialog.comboBox.getSelectedObjects();
			if (selected.length == 0)
				return null;
			else
				return (EditableProfileType) selected[0];
		}
	}



	private class ProfileTypeRenderer extends JLabel
	implements ListCellRenderer {

		ProfileTypeRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}

		@Override
		public Component getListCellRendererComponent(
				JList list,
				Object value,
				int index,
				boolean isSelected,
				boolean cellHasFocus) {

			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			if (value == null) {
				setText("");
			} else {
				EditableProfileType profileType = (EditableProfileType) value;
				setText(profileType.getName(msgs));
			}
			return this;
		}
	}

}
