package menalea.profile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import menalea.Message;
import menalea.MessageBundle;

/**
 * An enum of the read-only profiles.
 *
 */
public enum ReadOnlyProfileType {

	DEFAULT(Message.menuEditSelectAll, DefaultProfile.INSTANCE),
	;


	public final Message nameMessage;
	public final ReadOnlyProfile profile;


	private ReadOnlyProfileType(Message name, ReadOnlyProfile profile) {
		this.nameMessage = name;
		this.profile = profile;
	}


	public String getName(MessageBundle bundle) {
		return bundle.get(nameMessage);
	}


	public static List<ReadOnlyProfileType> getList() {
		return Arrays.asList(values());
	}

	public static List<ReadOnlyProfile> getProfileList() {
		List<ReadOnlyProfile> l = new ArrayList<ReadOnlyProfile>();
		for (ReadOnlyProfileType t : getList()) {
			l.add(t.profile);
		}
		return l;
	}

}
