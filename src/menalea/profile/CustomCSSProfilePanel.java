package menalea.profile;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import menalea.Message;
import menalea.MessageBundle;

@SuppressWarnings("serial")
public class CustomCSSProfilePanel extends ProfilePanel {

	private final CustomCSSProfile profile;

	private final JTextArea textArea;



	public CustomCSSProfilePanel(CustomCSSProfile profile, MessageBundle msgs) {

		this.profile = profile;

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		textArea = new JTextArea();


		{
			{
				JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
				p.add(new JLabel(msgs.get(Message.profileDialogCssRules)));
				add(p);
			}
			JScrollPane scrollPane = new JScrollPane(textArea);
			scrollPane.setPreferredSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
			add(scrollPane);
		}
	}



	@Override
	public EditableProfile getProfile() {
		return profile;
	}

	@Override
	public void apply() {
		profile.setCSS(textArea.getText());
	}

	@Override
	public void update() {
		textArea.setText(profile.getCSS());
	}



}
