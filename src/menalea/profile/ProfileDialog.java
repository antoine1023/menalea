package menalea.profile;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import menalea.Message;
import menalea.MessageBundle;

@SuppressWarnings("serial")
public class ProfileDialog extends JDialog {

	private final EditableProfile profile;
	private final ProfilePanel profilePanel;
	private final JTextField nameTextField;
	private final JButton okButton, closeButton;



	public ProfileDialog(Window parent, EditableProfile profile, MessageBundle msgs) {
		this(parent, ProfilePanel.createPanel(profile, msgs), msgs);
	}

	public ProfileDialog(Window parent,
			final ProfilePanel profilePanel,
			final MessageBundle msgs) {

		super(parent, ModalityType.APPLICATION_MODAL);

		this.profile = profilePanel.getProfile();
		this.profilePanel = profilePanel;

		setSize(600, 300);
		setMinimumSize(getSize());
		setLocationRelativeTo(null);

		setTitle(msgs.get(Message.profileDialogTitle, profile.getName()));


		nameTextField = new JTextField(10);

		okButton = new JButton(msgs.get(Message.okButton));
		okButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				apply();
				setVisible(false);
			}
		});
		rootPane.setDefaultButton(okButton);

		closeButton = new JButton(msgs.get(Message.closeButton));
		closeButton.addActionListener(new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		{
			Box box = Box.createVerticalBox();
			{
				JPanel field = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				field.add(new JLabel(msgs.get(Message.profileDialogName)));
				field.add(nameTextField);
				box.add(field);
			}
			box.add(profilePanel);
			add(box, BorderLayout.CENTER);
		}

		{
			JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonPanel.add(closeButton);
			buttonPanel.add(okButton);
			add(buttonPanel, BorderLayout.SOUTH);
		}

		update();
	}



	private void update() {
		nameTextField.setText(profile.getName());
		profilePanel.update();
	}

	private void apply() {
		// TODO check if another profile is similarly named
		profile.setName(nameTextField.getText().trim());
		profilePanel.apply();
	}



	public static void editProfile(Window parent, EditableProfile profile,
			MessageBundle msgs) {

		ProfileDialog dialog = new ProfileDialog(parent, profile, msgs);
		dialog.setVisible(true);
	}

}
