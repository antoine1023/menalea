package menalea.profile;

import menalea.Message;
import menalea.MessageBundle;

public enum EditableProfileType {

	SIMPLE(Message.profileTypeSimple),
	CUSTOM_CSS(Message.profileTypeCustomStyleSheet),
	;


	public final Message nameMessage;



	private EditableProfileType(Message name) {
		this.nameMessage = name;
	}



	public String getName(MessageBundle bundle) {
		return bundle.get(nameMessage);
	}
}