package menalea.profile;

import menalea.StyleSheetBuilder;

public class CustomCSSProfile extends EditableProfile implements StyleSheetBuilder {

	private String css = "";



	public CustomCSSProfile() {
	}



	@Override
	public String getCSS() {
		return css;
	}
	public void setCSS(String css) {
		this.css = css;
		notifyChange();
	}



	@Override
	public EditableProfileType getType() {
		return EditableProfileType.CUSTOM_CSS;
	}

}
