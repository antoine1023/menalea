package menalea.profile;

import menalea.MessageBundle;
import menalea.StyleSheetBuilder;

public abstract class Profile implements StyleSheetBuilder {

	public Profile() {
	}

	/**
	 * 
	 * @param msgs
	 * @return The profile name.
	 */
	public abstract String getName(MessageBundle msgs);

}
