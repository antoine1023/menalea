package menalea.profile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import menalea.Message;
import menalea.MessageBundle;
import menalea.Tools;

public final class DefaultProfile extends ReadOnlyProfile {

	public static final DefaultProfile INSTANCE = new DefaultProfile();

	private static final String[] IMPORTED_URLS;

	static {
		String base = "http://fonts.googleapis.com/css?family=";

		IMPORTED_URLS = new String[] {
				base + "Roboto+Slab:700",
				base + "Noto+Sans:400,700,400italic,700italic",
				base + "Droid+Serif",
		};
	}


	public DefaultProfile() {
	}


	@Override
	public String getCSS() {
		return getCSS(getImportedUrls(), 13f, "Noto Sans", "Roboto Slab");
	}

	@Override
	public String getName(MessageBundle msgs) {
		return msgs.get(Message.profileReadOnlyDefaultName);
	}


	@Override
	public ReadOnlyProfileType getType() {
		return ReadOnlyProfileType.DEFAULT;
	}




	public static String getCSSTemplate() {
		try {
			InputStream is = Tools.class.getResourceAsStream("default.css");
			String css = Tools.readUTF8(is);
			return css;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	public static String getCSS(
			List<String> importedUrls,
			float fontSize,
			String fontFamily,
			String headingFontFamily) {

		String cssTemplate = DefaultProfile.getCSSTemplate();

		String importsString = "";
		for (String url : importedUrls) {
			importsString += "@import url(" + url + ");\n";
		}

		String css = importsString + cssTemplate.
				replace("TEXT_FONT_SIZE", fontSize + "pt").
				replace("TEXT_FONT_FAMILY", fontFamily).
				replace("HEADING_FONT_FAMILY", headingFontFamily);

		return css;
	}

	/**
	 * @return The imported URLS of the style sheet.
	 */
	public static List<String> getImportedUrls() {
		return Arrays.asList(IMPORTED_URLS);
	}
}
