package menalea.profile;

import java.util.List;


public class SimpleProfile extends EditableProfile {

	/**
	 * The paragraph font size in points
	 */
	private float fontSize = 13f;

	/**
	 * The paragraph font family
	 */
	private String fontFamily = "Noto Sans";

	/**
	 * The heading font family
	 */
	private String headingFontFamily = "Roboto Slab";


	// TODO Users should be able to modify this.
	private final List<String> importUrls = DefaultProfile.getImportedUrls();



	public SimpleProfile() {
	}



	public float getFontSize() {
		return fontSize;
	}
	/**
	 * @param size The paragraph font size in points
	 */
	public void setFontSize(float size) {
		this.fontSize = size;
		notifyChange();
	}


	public String getFontFamily() {
		return fontFamily;
	}
	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
		notifyChange();
	}


	public String getHeadingFontFamily() {
		return headingFontFamily;
	}
	public void setHeadingFontFamily(String headingFontFamily) {
		this.headingFontFamily = headingFontFamily;
		notifyChange();
	}



	@Override
	public String getCSS() {
		return DefaultProfile.getCSS(importUrls,
				fontSize, fontFamily, headingFontFamily);
	}



	@Override
	public EditableProfileType getType() {
		return EditableProfileType.SIMPLE;
	}
}
