package menalea.profile;

import javax.swing.JPanel;

import menalea.MessageBundle;

@SuppressWarnings("serial")
public abstract class ProfilePanel extends JPanel {

	public abstract EditableProfile getProfile();

	/**
	 * Saves the changes to the model.
	 */
	public abstract void apply();

	public abstract void update();


	public static ProfilePanel createPanel(EditableProfile p, MessageBundle msgs) {
		switch (p.getType()) {
		case CUSTOM_CSS:
			return new CustomCSSProfilePanel((CustomCSSProfile) p, msgs);

		case SIMPLE:
			return new SimpleProfilePanel((SimpleProfile) p, msgs);

		default:
			throw new RuntimeException();
		}
	}
}
