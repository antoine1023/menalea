package menalea.profile;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import menalea.MessageBundle;

public class ProfileManager implements EditableProfile.Listener {

	private final List<Listener> listeners = new ArrayList<Listener>();


	private final ArrayList<EditableProfile> editableProfiles;
	private Profile current;



	public ProfileManager() {
		editableProfiles = new ArrayList<EditableProfile>();

		current = DefaultProfile.INSTANCE;
	}

	public ProfileManager(XMLDecoder decoder) {
		Proxy proxy = (Proxy) decoder.readObject();

		editableProfiles = proxy.getEditableProfiles();
		for (EditableProfile p : editableProfiles) {
			p.addListener(this);
		}

		setCurrent(proxy.getCurrent());
	}



	public EditableProfile get(String name) {
		for (EditableProfile profile : editableProfiles) {
			if (profile.getName().equals(name))
				return profile;
		}
		return null;
	}

	public boolean contains(String name) {
		return get(name) != null;
	}

	public boolean contains(EditableProfile target) {
		for (EditableProfile profile : editableProfiles) {
			if (profile == target)
				return true;
		}
		return false;
	}


	private String getNewProfileName(String namePrefix) {

		int i = 1;

		String name = namePrefix;

		while(contains(name)) {
			name = namePrefix + " " + i;
			i++;
		}

		return name;
	}

	/**
	 * Adds the profile and sets its name.
	 * @param profile The profile to add.
	 */
	private void add(EditableProfile profile) {
		if (contains(profile))
			throw new IllegalArgumentException();

		editableProfiles.add(profile);

		profile.addListener(this);

		for (Listener l : listeners) {
			l.profileAdded(this);
		}
	}


	public EditableProfile add(EditableProfileType type, String namePrefix) {
		EditableProfile profile = null;

		switch(type) {
		case CUSTOM_CSS:
			profile = new CustomCSSProfile();
			break;

		case SIMPLE:
			profile = new SimpleProfile();
			break;

		default:
			throw new RuntimeException();
		}

		profile.setName(getNewProfileName(namePrefix));
		add(profile);

		return profile;
	}



	public void remove(EditableProfile profile) {

		if (current == profile)
			throw new RuntimeException("Cannot remove the default profile");

		if (! contains(profile))
			throw new IllegalArgumentException();

		editableProfiles.remove(profile);

		for (Listener l : listeners) {
			l.profileRemoved(this);
		}
	}



	public Profile getCurrent() {
		return current;
	}

	public void setCurrent(EditableProfile profile) {
		if (! contains(profile))
			throw new IllegalArgumentException();
		current = profile;

	}

	public void setCurrent(Profile profile) {
		if (profile instanceof EditableProfile) {
			setCurrent((EditableProfile) profile);
		} else {
			current = profile;
		}
	}



	public ArrayList<EditableProfile> getEditableProfiles() {
		return new ArrayList<EditableProfile>(editableProfiles);
	}

	/**
	 * @return Editable profiles plus read-only profiles.
	 */
	public ArrayList<Profile> getAllProfiles() {
		ArrayList<Profile> l = new ArrayList<Profile>(
				ReadOnlyProfileType.getProfileList());

		l.addAll(editableProfiles);
		return l;
	}



	public void write(XMLEncoder encoder) {
		Proxy proxy = new Proxy();
		proxy.setCurrent(current);
		proxy.setEditableProfiles(editableProfiles);
		encoder.writeObject(proxy);
	}



	@Override
	public void profileChanged(EditableProfile profile) {
		for (Listener l : listeners) {
			l.profileChanged(this);
		}
	}



	public void addListener(Listener listener) {
		listeners.add(listener);
	}
	public void removeListener(Listener listener) {
		listeners.remove(listener);
	}



	public static interface Listener {
		public void profileAdded(ProfileManager manager);
		public void profileRemoved(ProfileManager manager);
		public void profileChanged(ProfileManager manager);
	}



	public static ProfileManager readFromPreferences(MessageBundle msgs) {
		Preferences prefs = Preferences.userNodeForPackage(
				ProfileManager.class);

		String xml = prefs.get("profiles", null);
		if (xml == null) {
			return new ProfileManager();
		} else {
			InputStream is = new ByteArrayInputStream(xml.getBytes());
			XMLDecoder decoder = new XMLDecoder(is);
			try {
				return new ProfileManager(decoder);
			} catch (Exception e) {
				e.printStackTrace();
				prefs.remove("profiles");
				return new ProfileManager();
			}
		}
	}



	public void writeToPreferences() {
		Preferences prefs = Preferences.userNodeForPackage(
				ProfileManager.class);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(baos);
		encoder.setExceptionListener(new ExceptionListener() {
			@Override
			public void exceptionThrown(Exception e) {
				e.printStackTrace();
			}
		});
		write(encoder);
		encoder.close();
		String s = baos.toString();
		prefs.put("profiles", s);
	}



	public static class Proxy {

		private ArrayList<EditableProfile> editableProfiles;

		/**
		 * The read-only current profile type, or <code>null</code> if the
		 * current profile is editable.
		 */
		private ReadOnlyProfileType currentReadOnlyProfileType;

		/**
		 * The current profile or <code>null</code> if the current profile
		 * is read-only.
		 */
		private EditableProfile currentEditableProfile;


		public Proxy() {
		}


		public ArrayList<EditableProfile> getEditableProfiles() {
			return editableProfiles;
		}
		public void setEditableProfiles(
				ArrayList<EditableProfile> editableProfiles) {
			this.editableProfiles = editableProfiles;
		}

		public Profile getCurrent() {
			if (currentEditableProfile != null) {
				return currentEditableProfile;
			} else if (currentReadOnlyProfileType != null) {
				return currentReadOnlyProfileType.profile;
			} else {
				return null;
			}
		}
		public void setCurrent(Profile current) {
			if (current instanceof ReadOnlyProfile) {
				ReadOnlyProfile rop = (ReadOnlyProfile) current;
				currentEditableProfile = null;
				currentReadOnlyProfileType = rop.getType();
			} else {
				currentEditableProfile = (EditableProfile) current;
				currentReadOnlyProfileType = null;
			}
		}

	}
}
