package menalea.profile;

public abstract class ReadOnlyProfile extends Profile {

	public abstract ReadOnlyProfileType getType();

}
